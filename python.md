# Python practices
## pdb debugging [cheatsheet](https://github.com/nblock/pdb-cheatsheet)
c(continue), n(next), s(step into), q(quit), r(continue until the function returns)

## reading files
> fp = open("filename", "r")  
> with open("filename") as fp

## no operation
> pass