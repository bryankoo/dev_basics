# dev_basics

Not a software development project but a documentations for development projects.  
Create separate documents and link here for long explanation.

## markdown [cheat sheet](https://www.markdownguide.org/cheat-sheet/)
* line break 2 spaces
* list *
* headings #, ##, ###
* blockquotes >

## ubuntu setup after installation
* sudo apt install vim, then setup ~/.vimrc for tab setting
* sudo apt install gparted, then format for additinal disk
* make mount directory and run mount command for additinal disk

## vi
* block indenting : v로 블럭을 잡고 <>로 이동시킨다
* move between braces : %
* window creation and move : ctrl-w v, ctrl-w jk
* search and replace : 1,$ s/search/replage/g
* multiple vertical files: vi -O (ctrl w w for toggle window)
* multiple scroll: set scrollbind
* search starts/ends with : \^word word$

## shell commands
* find . -name "cmudict*" -print
* find . -name "*java" -exec grep -H "word" {} \;

## etc

* terminal
  * new tab crtl-shft-t
  * new window ctrl-shft-n
* mysqld 재시작 sudo /etc/init.d/mysql restart
* swagger is for api definition
* postman if for api test
* python package 설치: sudo pip install requests
* [jupyter notebook cheat sheet](https://www.cheatography.com/weidadeyue/cheat-sheets/jupyter-notebook/pdf_bw/)