# Crawling and scraping
There are ways to crawl or scrape data from Internet. (Python/JS)

## Python http (http request + html parser)
install/use urllib or urllib2 or requests for the http request  
install/use beautifulsoup for the parser

```python
from bs4 import BeautifulSoup
import requests
from fake_useragent import UserAgent

ua = UserAgent()
headers = { "user-agent" : ua.random, }
url = "https://dictionary.goo.ne.jp/word/en/abed"
sess = requests.Session()
resp = sess.get(url, headers= headers)
soup = BeautifulSoup(resp.content, "lxml")
divs = soup.find("div")
text = divs[0].text
```

## Python with browser (selenium + browser)
intall/use selenium webdriver for the browser interface  
install/use phantomjs for the headless browser  
an example is included in the project as gtrans.py

```python
from selenium import webdriver

driver = webdriver.PhantomJS()
driver.implicitly_wait(5)
driver.get("https://translate.google.com#en/ja/")
src_elem = driver.find_element_by_id("source")
src_elem.clear()
src_elem.send_keys(src)
```
## Python framework (good for periodic execution, scrapy)

## JS with browser (puppeteer or casper)