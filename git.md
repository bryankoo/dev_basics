* 백지 상태에서 기존 소스를 처음 가져올때
  * git clone git_url
* 소스/데이터만 그냥 다른 컴퓨터에 복사한 경우
  * git status 명령으로 git 저장소 없음 확인
  * git init
  * git remote add origin https://github.com/BryanKoo/PATANA.git
  * git push --set-upstream origin master
  * git pull origin master --allow-unrelated-histories
* 코드 수정하거나 배포하기 전에 다른 사람이 고친 것을 포함한 최근 버전으로 업데이트
  * git pull
  * git pull origin master
* 충돌시 상태 및 내용 확인
  * git status, git diff
* 코드 수정 후 배포
  * git add . >> git commit -m " " >> git push
* 배포시 아이디 패스워드 생략
  * git config --global credential.helper 'cache --timeout 7200'
  * git config credential.helper store
* 브랜치를 생성할때
  * git branch br >> git checkout br
  * 리모트에도 동일 브랜치를 생성한다.
* 브랜치를 배포할때
  * git push origin br
  * 머지 리퀘스트를 한다.
* 용량이 큰 파일은 lfs로 등록
  * git lfs track *.ko *.zh *.kr
* 파일명 변경
  * git mv oldfile newfile
  * 이미 파일명을 바꾸었다면 삭제
    * git rm oldfile
    * git add newfile

## gitlab project 새로 만든 예제

Git global setup

* git config --global user.name "Kyeonghoon Koo"
* git config --global user.email "khkoo@sk.com"

Create a new repository from a existing git source

* git clone http://175.126.56.233/khkoo/techdoc_crawler.git
* cd techdoc_crawler
* touch README.md
* git add README.md
* git commit -m "add README"
* git push -u origin master

Existing folder or Git repository

* cd existing_folder
* git init
* git remote add origin http://175.126.56.233/khkoo/techdoc_crawler.git
* git add .
* git commit
* git push -u origin master