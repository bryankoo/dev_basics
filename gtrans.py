# -*- coding: utf-8 -*-
import sys, pdb
reload(sys)
sys.setdefaultencoding('utf-8')
import time
import io
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import codecs

driver = None

def create_driver(source, target):
  global driver
  if driver is not None:
    driver.quit()
  driver = webdriver.PhantomJS()
  driver.implicitly_wait(5)
  gtrans_url = "https://translate.google.com#" + source + "/" + target + "/"
  driver.get(gtrans_url)
  sys.stderr.write("CREATE DRIVER\n")

if __name__ == "__main__":

  if len(sys.argv) < 4:
    print "run with tree arguments for the source language and the target language and the source text to translate"
    sys.exit()

  source = sys.argv[1]
  target = sys.argv[2]
  filename = sys.argv[3]
  fp = open(filename, 'r')
  lines = fp.readlines()
  fp.close()

  for line in lines:
    if driver is None:
      create_driver(source, target)

    try_count = 0
    while try_count < 3:
      try_count += 1
      #pdb.set_trace()
      try:
        src = line.strip().decode("utf-8")
        src_elem = driver.find_element_by_id("source")
        src_elem.clear()
        time.sleep(0.5)
        src_elem.send_keys(src)
        time.sleep(2.0)
        target_elems = driver.find_elements_by_css_selector("#result_box span")
        translated = " ".join([elem.text for elem in target_elems])
        print(src + "|||" + translated)
        sys.stdout.flush()
        break
      except Exception as e:
        sys.stderr.write("FAILED: " + src + "\n")
        driver.quit()
        driver = None
        create_driver()

  driver.quit()
